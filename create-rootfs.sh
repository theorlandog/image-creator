#! /bin/bash

#
# Author: Badr BADRI © pythops
#

set -e

ARCH=arm64

PLATFORM=$1
RELEASE=$2
IMAGE_ROOTFS_DIR=tmp/$PLATFORM-root


if [ "$PLATFORM" = "nano" ] || [ "$PLATFORM" = "rpi4" ]; then
	echo "Create image for $PLATFORM"
else
	echo "Error $PLATFORM not supported"
fi
	

# Check if the user is not root
if [ "x$(whoami)" != "xroot" ]; then
	printf "\e[31mThis script requires root privilege\e[0m\n"
	exit 1
fi

# Check for env variables
if [ ! $IMAGE_ROOTFS_DIR ]; then
	printf "\e[31mYou need to set the env variable \$IMAGE_ROOTFS_DIR\e[0m\n"
	exit 1
fi

# Install prerequisites packages
printf "\e[32mInstall the dependencies...  "
apt-get update > /dev/null
apt-get install --no-install-recommends -y qemu-user-static debootstrap coreutils parted wget gdisk e2fsprogs > /dev/null
printf "[OK]\n"

# Create rootfs directory
printf "Create rootfs directory...    "
mkdir -p $IMAGE_ROOTFS_DIR
printf "[OK]\n"

# Download ubuntu base image
if [ ! "$(ls -A $IMAGE_ROOTFS_DIR)" ]; then
	printf "Download the base image...   "
	if [ "$PLATFORM" = "nano" ]; then
		wget -qO- http://cdimage.ubuntu.com/ubuntu-base/releases/20.04/beta/ubuntu-base-20.04-beta-base-arm64.tar.gz | tar xzvf - -C $IMAGE_ROOTFS_DIR > /dev/null
	elif [ "$PLATFORM" = "rpi4" ]; then
		#wget -qO-  http://cdimage.ubuntu.com/releases/20.04/beta/ubuntu-20.04-beta-preinstalled-server-arm64+raspi.img.xz | xz --decompress > ubuntu-20.04-beta-preinstalled-server-arm64+raspi.img
		#mkdir -p tmp/rpi4-loop
		#mount -o mount -o loop,offset=269484032 ubuntu-20.04-beta-preinstalled-server-arm64+raspi.img tmp/rpi4-loop
		cp -aR tmp/rpi4-loop/* $IMAGE_ROOTFS_DIR
		umount tmp/rpi4-loop
	fi
	printf "[OK]\n"
else
	printf "Base image already downloaded"
fi

# Run debootsrap first stage
printf "Run debootstrap first stage...  "
debootstrap \
        --arch=$ARCH \
        --keyring=/usr/share/keyrings/ubuntu-archive-keyring.gpg \
        --foreign \
        --variant=minbase \
	--include=python3,libegl1,python3-apt \
        $RELEASE \
	$IMAGE_ROOTFS_DIR > /dev/null
printf "[OK]\n"

# Copy qemu-aarch64-static
cp /usr/bin/qemu-aarch64-static $IMAGE_ROOTFS_DIR/usr/bin

# Run debootstrap second stage
printf "Run debootstrap second stage... "
chroot $IMAGE_ROOTFS_DIR /bin/bash -c "/debootstrap/debootstrap --second-stage"  > /dev/null
printf "[OK]\n"

printf "Success!\n"
